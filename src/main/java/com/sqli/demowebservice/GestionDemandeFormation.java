package com.sqli.demowebservice;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.sqli.demowebservice.domaine.DemandeFormation;
import org.apache.cxf.feature.Features;

@WebService
@Features(features = "org.apache.cxf.feature.LoggingFeature") 
public interface GestionDemandeFormation {

    @WebMethod
    String traitementDemande(DemandeFormation demande);
}
